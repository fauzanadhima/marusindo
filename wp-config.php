<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'marusindo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'g;W2bf-?DbWl.M^W9FA9n8+v<}]gP br6IqyS|%`g,t+~61vFv<=ou*o$.A}*N?s' );
define( 'SECURE_AUTH_KEY',  '@$ Z*~[am{JI.uHqd{Hy]4)?Ela)>v`2>o-bf&~@33q.(hhPAh.NlbywVm^<~-pR' );
define( 'LOGGED_IN_KEY',    'Z$i#.6Z}iHTkRbz#Mv c(`BRlCScw0PvO2i)Z>&VMWg*~OX<7Ys3jCf.8 |GT7dN' );
define( 'NONCE_KEY',        '&N`:JOs>1rfWXkN5m1+yRS>544f)T!t+x|gJ?DeK-$t)?gK]FC3gPOu^a$w,@K9b' );
define( 'AUTH_SALT',        '#mqRySC?.-mYM-q:Kgh[CmPY$Rfe{vLnfq&<%RhlAG(4p)qkLOJu9>dKB,1H-a*5' );
define( 'SECURE_AUTH_SALT', ']qRk=61s=_o[rpW8(<D8l<vpbEfga+<a_2H;Xry`Pnu/G}{K,u~E$$%v5_Sgb:[n' );
define( 'LOGGED_IN_SALT',   'g6,xrfm8hNuw=s73hvt8lX d]h3I.dWgyhv*vQG:>t0<R4p02RXY*K-|4S/t%W[l' );
define( 'NONCE_SALT',       'aB%PSr`9CVdlos)y[vf>ox8RvCB3$;/[o!U*v`xo 5TIou_|wzvfXDhBh>%W/h|i' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
